FROM nikolaik/python-nodejs:python3.11-nodejs18-slim
EXPOSE 3006
WORKDIR /app
COPY package*.json .
COPY requirements.txt .
RUN pip install  --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org  --no-cache-dir -r requirements.txt
RUN npm config set strict-ssl false && npm install
COPY tsconfig.json .
COPY update.py .
COPY src src
WORKDIR /app

CMD [ "npm", "run" , "start" ]
