# Deutsche Gesetze Microservice

Stellt den Inhalt [von ca 15k deutscher Gesetze](https://www.gesetze-im-internet.de/) über einen REST Endpunkt zur Verfügung.

z.B.: http://localhost/§%201%20BGB 👇
```html
<h1>§ 1 BGB - Beginn der Rechtsfähigkeit</h1><p>Die Rechtsfähigkeit des Menschen beginnt mit der Vollendung der Geburt.</p>
```


## Installation
Python packages installieren:
```bash
pip install -r requirements.txt
```
JS packages installieren:
```bash
npm i
```


## Entwicklung
```bash
npm run dev
```


## Test
```bash
npm run test
```
