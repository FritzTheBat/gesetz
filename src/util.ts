import path from 'path'
import fs from 'fs'

import pkg from 'glob'; //  'glob' is a CommonJS module
const { glob } = pkg;


export async function getLatestFileUpdate(){
    // get newest file from laws folder
    const allFiles = glob(path.resolve('laws') + '/**/*.xml', {sync: true}) 
    const latestFileMod = getNewestFile(allFiles)
    return latestFileMod
}

function getNewestFile(files: string[]): Date | null {
    var out = []
    files.forEach(file => { 
        file = path.resolve(file) 
        var stats = fs.statSync(file)
        if(stats.isFile()) {
            out.push({"file":file, "mtime": stats.mtime.getTime()})
        }
    })
    out.sort(function(a,b) {
        return b.mtime - a.mtime
    })

    return out.length ? new Date(out[0].mtime) : null
}