export class NoBookFoundError extends Error {
    constructor() {
        super("Gesetz nicht gefunden");
        this.name = "NoBookFoundError";
    }
}

export class SectionDoesntExist extends Error {
    constructor() {
        super("Paragraph existiert nicht");
        this.name = "SectionDoesntExist";
    }
}