import {describe, it, expect} from "vitest"
import { getHtml, getObj } from "./read.js"
import { getLatestFileUpdate } from "./util.js"
import { SectionDoesntExist } from "./errors.js"

describe("read", () => {

   
    // happy path
    it("should know the bgb", () => {
        const res = getObj("§ 433 BGB")
        expect(res[0].ueberschrift).toContain("Vertragstypische Pflichten beim Kaufvertrag")
        expect(res[0].inhalt).toContain("Sach- und Rechtsmängeln zu verschaffen.</p><p>(2) Der Käufer ist verpflichtet,")
    })

    it("should highlight absatz", () => {
        const res = getObj("§ 631 Abs.2 BGB")
        expect(res[0].ueberschrift).toContain("Vertragstypische Pflichten beim Werkvertrag")
        expect(res[0].inhalt).toContain("<em")
    })

    it("should highlight satz", () => {
        const res = getObj("§ 535 Abs.1 Satz 2 BGB") 
        expect(res[0].ueberschrift).toContain("Inhalt und Hauptpflichten des Mietvertrags")
        expect(res[0].inhalt).toContain("<em")
    })

    
    it("handle simple paragraph chain", () => {
        const res = getHtml("§§ 437, 439 BGB")
        expect(res).toContain("Rechte des Käufers bei Mängeln")
        expect(res).toContain("<h1>§ 439 BGB - Nacherfüllung</h1><p>(1) Der Käufer kan")
    })

    // unhappy path
    it("should throw 'existiert nicht' when PARAGRAPH was not found", () => {
        expect(async() => await getObj("§ 3000 BGB")).rejects.toThrowError(/existiert nicht/)
    })

    it("should return 'existiert nicht' when ABSATZ was not found", () => {
        const res = getObj("§ 433 Abs.8 BGB")
        expect(res[0].ueberschrift).toContain("Vertragstypische Pflichten beim Kaufvertrag")
        expect(res[0].inhalt).toContain("existiert nicht")
    })
})  

describe("util", () => { 
    it("should get latest file", async () => {
        const res = await getLatestFileUpdate()
        expect(res).toBeDefined()
    })
})