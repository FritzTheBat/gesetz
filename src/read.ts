import { XMLParser} from "fast-xml-parser"
import { readFileSync } from "fs"
import path from "path"
import { fileURLToPath } from 'url';
import { NoBookFoundError, SectionDoesntExist } from "./errors.js";
import {citations} from "./citations.js";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


export interface Fundstelle {
    metadaten: Metadaten;
    textdaten: Textdaten;
}

export interface Metadaten {
    jurabk: string;
    enbez:  string;
    titel:  string;
}

export interface Textdaten {
    text:      Text;
    fussnoten: string;
}

export interface Text {
    Content: Content;
}


export interface Content {
    P: string;
}

interface NormParsed {
    paragraph: string;
    paragraph2: string; // paragraph chain / Paragraphenkette
    absatz: string;
    satz: string;
    nummer?: string;
    buch: string;
    titel: string;
}



export function getHtml(fundstelle: string): string | null {
    const fundstelleObj = getObj(fundstelle)
    if(!fundstelleObj) return null
    return fundstelleObj.map(obj => {
        return "<h1>"+obj.ueberschrift+"</h1>"+obj.inhalt
    }).join("<hr>")
}

export function getObj(fundstelle: string): { ueberschrift: string, inhalt: string }[] | null {
    const fundstelleParsed = parseNorm(fundstelle)


    const chain = [readXml(fundstelleParsed.buch, fundstelleParsed.paragraph)]
    if(fundstelleParsed.paragraph2) {
        chain.push(readXml(fundstelleParsed.buch, fundstelleParsed.paragraph2))
    }

    return chain.map((section, i) => {  
        if(!section) throw new SectionDoesntExist() 

        const p = Array.isArray(section.textdaten.text.Content.P) ? section.textdaten.text.Content.P : [section.textdaten.text.Content.P]
        if(i == 0){ // currently regex only supports one absatz in a chain
            applyHighlighting(p, parseInt(fundstelleParsed.absatz), parseInt(fundstelleParsed.satz))
        }
        const inhalt = "<p>" + p.join("</p><p>") + "</p>" 
        return {ueberschrift: section.metadaten.enbez+" "+ section.metadaten.jurabk + " - " + section.metadaten.titel, inhalt}
    })
    
}

function applyHighlighting(p: string[], absatz: number, satz: number) {
    if(absatz && satz) {
        const absatzSaetze = p[absatz-1].split(/(?<![0-9]|Nr|Abs)\./)
        p[absatz-1] = p[absatz-1].replace(absatzSaetze[satz-1], "<em style=\"background: yellow\">" +
                    (absatzSaetze[satz-1]??`Absatz ${absatz} Satz ${satz} existiert nicht`) +
                    "</em>")
    } 
    else if(absatz) {
        p[absatz-1] = "<em style=\"background: yellow\">" +
            (p[absatz-1]??`Absatz ${absatz} existiert nicht`) +
            "</em>"
    } 
    // todo: no absatz but satz
}



function parseNorm(fundstelle: string): NormParsed | null {
    const regex = citations.getZitatRegex()
    const match = fundstelle.match(regex)
    if (match) {

        return{ 
            paragraph: match[2],
            absatz: match[5],
            satz: match[8],
            paragraph2: match[12],
            buch: match[13]??match[17],
            titel: match[18]
        }
    }
}

function readXml(buch: string, paragraph: string): Fundstelle {

    if(!buch) throw new NoBookFoundError()
    
    const parser = new XMLParser({stopNodes: ["dokumente.norm.textdaten.text.Content.P"]})
    const abbrev = buch.toLowerCase().replace(" ", "-")
    const fileLocation =  path.resolve(__dirname, "../laws/"+abbrev.slice(0,1)+"/"+abbrev+"/"+abbrev+".xml")
    console.log("fileLocation", fileLocation)        
    let jObj = parser.parse(readFileSync(fileLocation, "utf8"))

    return  jObj.dokumente.norm.find(n => n.metadaten.enbez == "§ "+paragraph) as Fundstelle
}