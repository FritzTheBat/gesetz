import express from 'express'
import { Request, Response, NextFunction } from 'express'
import { getHtml, getObj } from './read.js'
import { NoBookFoundError, SectionDoesntExist } from './errors.js'
import {citations} from "./citations.js";
import {spawn} from 'child_process'
import fs from 'fs'
import path from 'path'
import { fileURLToPath } from 'url';
import pkg from 'glob';
import { getLatestFileUpdate } from './util.js';
const { glob } = pkg;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


updateRegex()
downloadLatestGesetze().then(updateRegex)



const app = express()
console.log("starting server ..")
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use((req: Request, res: Response, next: NextFunction) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    res.set({ 'content-type': 'text/html; charset=utf-8' })
    next()
})

app.get('/regex', async function(req: Request, res: Response) {
    res.end(citations.getZitatRegex()+"")
})

app.get('/', async function(req: Request, res: Response) {

    const latestFileMod = await getLatestFileUpdate()
    res.set({ 'content-type': 'text/plain; charset=utf-8' })

    res.end("use /<gesetz> (i.e: /§ 1 BGB) and /regex endpoints\n" +
            "there are currently "+citations.lawCapt.split("|").length+" gesetze in our database\n" +
            // "use /update to update the database\n" +
            `last update: ${latestFileMod}\n`
            )
})

app.get('/:name', async function(req: Request, res: Response) {
    try{
        res.end(getHtml(req.params.name))
    }
    catch(e: any){
        if(e instanceof NoBookFoundError || e instanceof SectionDoesntExist){
            res.status(404).end(e.message)
        }
        else{
            console.error(e)
            res.status(500).end("internal server error")
        }
    }
})

var server = app.listen(3006, function() {
    console.log('Listening on port %s', server.address())
})



function downloadLatestGesetze(){
    return new Promise((resolve, reject) => {

        let running = true

        const notifyProgress = () => {

            const getDirectories = (src, callback) => { glob(src + '/**/*', callback)}
            getDirectories(path.resolve('laws'), (err, res) => {
                if (err) {
                    console.log('Error', err)
                } else {
                    console.log(`downloaded ${res.length} gesetze xml files`)
                    if(running){setTimeout(notifyProgress, 1000)}
                }
            });
        }

        console.log(`updating available gesetze ...`);
        const python = spawn('python', ['update.py', 'updatelist'])
        python.stdout.on('data', function (data) {
            console.log('Pipe data from python script ...', data.toString())
        });
        python.on('close', code => {
            notifyProgress()
            const python = spawn('python', ['update.py', 'loadall']);
            python.stdout.on('data', function (data) {
                console.log('Pipe data from python script ...', data.toString())
            });
            python.on('close', code => {
                console.log(`gesetze updated`)
                running = false
                resolve(true)
            });
            
        });
    })
}

function updateRegex(){
    try{
        const lawsJsonRaw = fs.readFileSync(path.resolve(__dirname, "../data/laws.json"), "utf-8")
        const lawsJson = JSON.parse(lawsJsonRaw)
        console.log("lawsJson contains", lawsJson.length, "books")
        const lawCapt = lawsJson.map((law: any) => law.abbreviation).join("|")
        citations.lawCapt = lawCapt
    }
    catch(e){
        console.error("could not load regex", e)
    }
}