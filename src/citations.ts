
class Citations{
    
    lawCapt: string = "BGB|ZPO|StGB|StPO|GG|GWB|UStG|EStG|AO|HGB|InsO|BauGB|VwGO|SGB"

    getZitatRegex(){
        return new RegExp("(§+|Art.) ([0-9]{1,4}[a-z]?) ?(f+.)? *(I*[V(?!V)X]I*a?|Ia|V?I+V?|Abs. ?([0-9]))?(?![a-z]) "
        + "*((S|Nr|Satz)?.? ?([0-9](?!\\d)))*( *([,-])? *(§ )*([0-9]{1,4}[a-z]?))? ?("+this.lawCapt+")?[^\\d§]*((§)+ ?([0-9]{1,4}[a-z]?))? ?("+this.lawCapt+")")
    }

}

export const citations = new Citations()